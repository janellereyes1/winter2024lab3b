import java.util.Scanner;

public class VirtualPetApp {

	public static void main(String[] args) {
	
	Scanner reader = new Scanner(System.in);
	
	Bunny[] fluffle = new Bunny[4];
	
	for(int i = 0; i < fluffle.length; i++) {
		fluffle[i] = new Bunny();
		
		System.out.println("Enter the bunnys colour");
		fluffle[i].colour = reader.nextLine();
		
		System.out.println("Enter the bunnys type");
		fluffle[i].type = reader.nextLine();
		
		System.out.println("Enter the bunnys speed");
		fluffle[i].speed = Integer.parseInt(reader.nextLine());
		
		System.out.println("");
	}
	
	System.out.println("The last bunny is: " + fluffle[3].colour);
	System.out.println("Its type is: " + fluffle[3].type);
	System.out.println("It has a speed of: " + fluffle[3].speed + " km/h");
	System.out.println("");
	
	System.out.println("The first bunny is...");
	fluffle[0].sleep();
	fluffle[0].eatCarrot();
	
	}
}