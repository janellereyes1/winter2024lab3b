public class Bunny {
	public String colour;
	public String type;
	public int speed;

	public void sleep() {
		System.out.println("Sleeping! zZz");
		
	}
	
	public void eatCarrot() {
		if(this.colour == "white"){
			System.out.println("So hungry... :(");
		}
		else {
			System.out.println("Eating a carrot!");
		}
	}
}